package com.DeletionOfFiles;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Properties;



/**
 * This Class will process the xml files in the folder path and send the xml
 * content to the DataBase and  upon response from DataBase xml file will be moved
 * to the ProcessedFiles/<Current Date> folder or UnProcessedFiles/<Current Date> folder.
 * 
 * 

 * 
 */
public class Deletion {

	
	static final String JDBC_DRIVER = "com.edb.Driver";  
	   static final String DB_URL = "jdbc:edb://192.168.0.82:5432/eureqa_lg";

	   //  Database credentials
	   static final String USER = "eureqaapp";
	   static final String PASS = "intxuser";
	/** The Constant LOGGER. */

	/** The sourcePath. */
	public static String sourcePath = null;
	public static Integer batchId = null;
	public static String targetPath = null;

	public ArrayList<String> al=new ArrayList<String>();

	/** The Constant fileType */	
	private static Properties properties = new Properties();


	

	/**
	 * The main method.
	 * 
	 * @param args
	 *          the arguments Takes 3 arguments 1: folder path 2.SourceIndicator 3.File type.
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * 
	 */
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		System.out.println("Program running...");
			int i;
			String userHome = System.getProperty("user.home");			 
			i=Integer.parseInt(args[0]);
			System.out.println(userHome);
			try {
				System.out.println("inside try block");
				FileReader reader = new FileReader(userHome+"/propertyFiles/Deletion.properties");
				Properties properties = new Properties();
				properties.load(reader);
				
				//properties.load(ClassLoader.getSystemClassLoader().getResourceAsStream(userHome+"/propertyFiles/Deletion.properties"));
				System.out.println("afteer property");
				if(i==1)
				{		
					System.out.println("inside if block");
					
				sourcePath=properties.getProperty("LeadGeneration.sourcePath.proc1");
				targetPath=properties.getProperty("LeadGeneration.targetPath.proc1");
				System.out.println(sourcePath);
				Deletion leadgeneration = new Deletion();
				leadgeneration.process();
				}
				else if(i==2)
				{
					System.out.println("inside else block");

					System.out.println("enter correct argument");
					sourcePath=properties.getProperty("LeadGeneration.sourcePath_proc3");
					System.out.println(sourcePath);

					Deletion leadgeneration = new Deletion();
					leadgeneration.process1();
				}
				else
				{
					System.out.println("please enter either 1 or 2 as argument only");
				}

			} catch (FileNotFoundException e) {
			} catch (IOException e) {

		}catch(NullPointerException e)
		{
		}
			catch(SQLException e)
			{
							}
		

	}

	/**
	 * Reading and Sending the XML file content to the Web Service and upon
	 * success moving the file to the different folder.
	 * 
	 * @param fileobj
	 *          the fileobj
	 * @param sb
	 *          the sb
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IOException 
	 */
	
	public void process1() throws ClassNotFoundException, SQLException, IOException {
	DataInputStream dis = 
		    new DataInputStream (
		    	 new FileInputStream (sourcePath));
 
		 byte[] datainBytes = new byte[dis.available()];
		 dis.readFully(datainBytes);
		 dis.close();
 
		 String content = new String(datainBytes, 0, datainBytes.length);
 
		 System.out.println(content);
		 Connection con = connect();
		   CallableStatement cstmt = null;
		      
		      con.setAutoCommit(false);
				cstmt = con.prepareCall("{call pkg_eureqa_maintenance.deletefilesdblocation(?)}");
				cstmt.setString(1, content);
				cstmt.execute();
				con.commit();

	}
	
	public void process() throws ClassNotFoundException, SQLException {
		try {


			 DataInputStream dis = 
					    new DataInputStream (
					    	 new FileInputStream (sourcePath));
			 
					 byte[] datainBytes = new byte[dis.available()];
					 dis.readFully(datainBytes);
					 dis.close();
			 
					 String content = new String(datainBytes, 0, datainBytes.length);
			 
					 System.out.println(content);
					 ArrayList<String> al1=new ArrayList<String>();
					 al1=DeleteFiles(content);
			System.out.println("ArrayList is "+al1);
			System.out.println("------writing into xml-----");
			File file = new File(targetPath);
			 System.out.println("batch id in process method: "+batchId);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write("<FileListForDelete>");
			for(int i=0;i<al1.size();i++)
			{
				bw.write("<file>");
				bw.write("<rawFileId>"+al1.get(i)+"</rawFileId>");
				bw.write("</file>");

			}
			bw.write("</FileListForDelete>");
			bw.close();
			
			DataInputStream dis1 = 
				    new DataInputStream (
				    	 new FileInputStream (targetPath));
		 
				 byte[] datainBytes1 = new byte[dis1.available()];
				 dis1.readFully(datainBytes1);
				 dis1.close();
		 
				 String content1 = new String(datainBytes1, 0, datainBytes1.length);
		 
				 System.out.println("content1 is :"+content1);
				 
				 Connection con = connect();
				   CallableStatement cstmt = null;
				      
				      con.setAutoCommit(false);
						cstmt = con.prepareCall("{call pkg_eureqa_maintenance.deletefilesphysicallocation(?,?)}");
						cstmt.setString(1, content1);
						cstmt.setInt(2, batchId);
						cstmt.execute();
						con.commit();
						System.out.println("2nd proc executed successfully with "+content1+" and "+batchId);
			
			
		}

		catch (IllegalArgumentException iae) {
			System.exit(0);
		}
		catch (FileNotFoundException fe) {
		}	 
		catch(IOException io) {
		}

	}
	/**
	 * Move files to ProcessedFiles/<Current Date> folder or UnProcessedFiles/<Current Date>.
	 * 
	 * @param fileobj
	 *          the fileobj
	 * @return true, if successful
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 */
	private Connection connect() throws SQLException, ClassNotFoundException{
		Connection con=null;
		//STEP 2: Register JDBC driver
			Class.forName("com.edb.Driver");
			// TODO Auto-generated catch block

	      //STEP 3: Open a connection
	      System.out.println("Connecting to database...");
	      con = DriverManager.getConnection(DB_URL,USER,PASS);

	      //STEP 4: Execute a query
	      System.out.println("Creating statement...");
	      
		return con;
	}
	private ArrayList<String> DeleteFiles(String metricsXML) throws ClassNotFoundException, SQLException {
		Connection con = connect();
		   CallableStatement cstmt = null;
		   try{
		      
		      con.setAutoCommit(false);
				cstmt = con.prepareCall("{call pkg_eureqa_maintenance.getfileinfotobedeleted(?,?,?,?,?)}");
				cstmt.setString(1, metricsXML);
				cstmt.registerOutParameter(2, Types.REF);
				cstmt.registerOutParameter(3, Types.REF);
				cstmt.registerOutParameter(4, Types.REF);
				cstmt.registerOutParameter(5, Types.INTEGER);
				cstmt.execute();
				con.commit();
				ResultSet rs = (ResultSet) cstmt.getObject(2);

		      //STEP 5: Extract data from result set
				System.out.println("\n\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Validations @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		      while(rs.next()){
		    	   
		    	  //Retrieve by column name
		                  System.out.print(rs.getString(1)+"--->");
		                  System.out.print(rs.getString(2)+"--->");
		                  System.out.println(rs.getString(3));
		                  
		                  /*if(rs.getString(3).equals("48083"))
		                  {*/
		                  File file = new File(rs.getString(2)+"/"+rs.getString(1));
				             if(file.exists()){
				            	 
				          		if(file.delete()){
				          			System.out.println(file.getName() + " is deleted!");
				          			
				          		}else{
				          			System.out.println(file.getName() +"Delete operation is failed.");
				          			al.add(rs.getString(3));
				          		}
				          		

				             }
				             else
				             {
				          			System.out.println(file.getName() +"--------> FILE DOESNT EXISTS");
				          			al.add(rs.getString(3));

				             }
		
		                  
		      }
		      rs.close();
		      batchId= (Integer)cstmt.getObject(5);
		      System.out.println("Batch id is :"+batchId);
		    ResultSet rs1 = (ResultSet) cstmt.getObject(3);

		      //STEP 5: Extract data from result set
				System.out.println("\n\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ runFiles @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n\n");
		      while(rs1.next()){
		         //Retrieve by column name
		                  System.out.print(rs1.getString(1)+"--->");
		                  System.out.print(rs1.getString(2)+"--->");
		                  System.out.println(rs1.getString(3)+"");

		                  //File file = new File(rs.getString(1));
		                  

	                	File file = new File(rs1.getString(2)+"/"+rs1.getString(1));
	                	System.out.println(file.getName());
	                	if(file.exists()){
			            	 
			          		if(file.delete()){
			          			System.out.println(file.getName() + " is deleted!");
			          			
			          		}else{
			          			System.out.println(file.getName() +"Delete operation is failed.");
			          			al.add(rs1.getString(3));
			          		}
			          		

			             }
			             else
			             {
			          			System.out.println(file.getName() +"--------> FILE DOESNT EXISTS");
			          			al.add(rs1.getString(3));

			             }
		    
		      }
		      rs1.close();

		      ResultSet rs2 = (ResultSet) cstmt.getObject(4);

		      //STEP 5: Extract data from result set
				System.out.println("\n\n\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ PDF Reports@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ \n\n");
		      while(rs2.next()){
		         //Retrieve by column name
		                  System.out.print(rs2.getString(1)+"--->");
		                  System.out.print(rs2.getString(2)+"--->");
		                  System.out.println(rs2.getString(3)+"");

		                  //File file = new File(rs.getString(1));

		              	File file = new File(rs2.getString(2)+"/"+rs2.getString(1));
			             if(file.exists()){
			            	 
			          		if(file.delete()){
			          			System.out.println(file.getName() + " is deleted!");
			          			
			          		}else{
			          			System.out.println(file.getName() +"Delete operation is failed.");
			          			al.add(rs2.getString(3));
			          		}
			          		

			             }
			             else{
			          			System.out.println(file.getName() +"--------> FILE DOESNT EXISTS");
			          			al.add(rs2.getString(3));

			             }
			             
		                  
		      }
		      //STEP 6: Clean-up environment
		      rs2.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(cstmt!=null)
		            cstmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(con!=null)
		            con.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");
		return al;
	
	}

	/**
	 * Creates the current date folder name.
	 * 
	 * @return the string
	 */
}
	